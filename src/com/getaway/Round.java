package com.getaway;

import com.sun.istack.internal.Nullable;

public class Round {

    private final int number;
    private Choice yourChoice;
    private Choice opponentChoice;
    private RoundResult result;

    public Round(int number, @Nullable Choice yourChoice, @Nullable Choice opponentChoice) {
        this.number = number;
        this.yourChoice = yourChoice;
        this.opponentChoice = opponentChoice;
    }

    public Choice getYourChoice() {
        return yourChoice;
    }

    public void setYourChoice(Choice yourChoice) {
        this.yourChoice = yourChoice;
    }

    public Choice getOpponentChoice() {
        return opponentChoice;
    }

    public void setOpponentChoice(Choice opponentChoice) {
        this.opponentChoice = opponentChoice;
    }

    public RoundResult getResult() {
        return result;
    }

    public void setResult(RoundResult result) {
        this.result = result;
    }

    public boolean roundPlayed(){
        return result != null;
    }

    @Override
    public String toString() {
        return "Round number "+number+" : "+yourChoice +" vs "+opponentChoice +" - "
                +(roundPlayed()? result.getDescription(): " yet to be played");
    }
}
