package com.getaway;

import java.util.Random;

public class GameAIServiceImpl implements GameAIService {

    private final Random rnd;

    public GameAIServiceImpl() {
        rnd = new Random();
    }

    @Override
    public int getNextMove(int possibleMoves) {
        return rnd.nextInt(possibleMoves);
    }
}
