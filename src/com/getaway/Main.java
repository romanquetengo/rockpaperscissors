package com.getaway;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {

    public static void main(String[] args) {
        Choice alwaysRock = Choice.ROCK;
        List<Round> rounds = new ArrayList<>(100);
        RoundResult[] possibleResults = RoundResult.values();
        Map<RoundResult, AtomicInteger> resultMap = new HashMap<>(possibleResults.length);

        for (RoundResult result: possibleResults){
            resultMap.put(result, new AtomicInteger(0));
        }
        for(int i = 0; i < 100; i++){
            rounds.add(new Round(i, alwaysRock, null));
        }
        rounds.parallelStream().forEach(round -> {
            RoundResult roundResult = MatchManager.SINGLETON.playRound(round);
            resultMap.get(roundResult).getAndIncrement();
        });
        System.out.println(resultMap);
    }
}
