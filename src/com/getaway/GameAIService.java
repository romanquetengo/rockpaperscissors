package com.getaway;

/*                                       ˜
* Let's pretend it might be a costly external service call
* */

public interface GameAIService {
    int getNextMove(int possibleMoves);
}
