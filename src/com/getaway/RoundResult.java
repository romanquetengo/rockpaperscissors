package com.getaway;

public enum RoundResult {

    WIN("You win this round"),
    DRAW("It's a draw"),
    LOSS("Well, you tried");


    private final String description;

    RoundResult(String description){
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
