package com.getaway;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public enum Choice {

    ROCK,
    PAPER,
    SCISSORS,
    WELL;


    private static Map<Choice, Set<Choice>> beatsMap = new HashMap<Choice, Set<Choice>>() {{
        put(ROCK, Collections.singleton(SCISSORS));
        put(PAPER, new HashSet<>(Arrays.asList(ROCK, WELL)));
        put(SCISSORS, Collections.singleton(PAPER));
        put(WELL, new HashSet<>(Arrays.asList(ROCK, SCISSORS)));
    }};

    RoundResult playAgainst(Choice opponentChoice) {
        if (this == opponentChoice) {
            return RoundResult.DRAW;
        } else if (beatsMap.get(this).contains(opponentChoice)) {
            return RoundResult.WIN;
        }
        return RoundResult.LOSS;

    }

}
