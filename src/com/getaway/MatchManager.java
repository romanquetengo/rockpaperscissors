package com.getaway;

import java.util.HashMap;
import java.util.Map;

public class MatchManager {
    public static MatchManager SINGLETON = new MatchManager();

    private final Choice[] possibleChoices;

    private GameAIService gameAIService;

    private MatchManager(){

        possibleChoices = Choice.values();
        gameAIService = new GameAIServiceImpl();
    }

    //Injection emulation
    void setGameAIService(GameAIService gameAIService) {
        this.gameAIService = gameAIService;
    }

    public Choice nextChoice(){
        int choice = gameAIService.getNextMove(possibleChoices.length);
        if(choice >= possibleChoices.length) {
            throw new IllegalArgumentException("Invalid choice: "+choice);
        }
        return possibleChoices[choice];
    }

    public RoundResult playRound(Round round){
        if(!round.roundPlayed()) {
            if (round.getYourChoice() == null) {
                round.setYourChoice(nextChoice());
            }
            if (round.getOpponentChoice() == null) {
                round.setOpponentChoice(nextChoice());
            }
            round.setResult(round.getYourChoice().playAgainst(round.getOpponentChoice()));
        }
        return round.getResult();
    }


}
