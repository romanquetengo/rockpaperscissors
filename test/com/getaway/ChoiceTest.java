package com.getaway;

import junit.framework.TestCase;

public class ChoiceTest extends TestCase {

    public void testPlayAgainst() {
        assertEquals(RoundResult.DRAW, Choice.ROCK.playAgainst(Choice.ROCK));
        assertEquals(RoundResult.DRAW, Choice.PAPER.playAgainst(Choice.PAPER));
        assertEquals(RoundResult.DRAW, Choice.SCISSORS.playAgainst(Choice.SCISSORS));
        assertEquals(RoundResult.DRAW, Choice.WELL.playAgainst(Choice.WELL));

        assertEquals(RoundResult.WIN, Choice.ROCK.playAgainst(Choice.SCISSORS));
        assertEquals(RoundResult.WIN, Choice.PAPER.playAgainst(Choice.ROCK));
        assertEquals(RoundResult.WIN, Choice.PAPER.playAgainst(Choice.WELL));
        assertEquals(RoundResult.WIN, Choice.SCISSORS.playAgainst(Choice.PAPER));
        assertEquals(RoundResult.WIN, Choice.WELL.playAgainst(Choice.ROCK));
        assertEquals(RoundResult.WIN, Choice.WELL.playAgainst(Choice.SCISSORS));

        assertEquals(RoundResult.LOSS, Choice.ROCK.playAgainst(Choice.PAPER));
        assertEquals(RoundResult.LOSS, Choice.ROCK.playAgainst(Choice.WELL));
        assertEquals(RoundResult.LOSS, Choice.PAPER.playAgainst(Choice.SCISSORS));
        assertEquals(RoundResult.LOSS, Choice.SCISSORS.playAgainst(Choice.ROCK));
        assertEquals(RoundResult.LOSS, Choice.SCISSORS.playAgainst(Choice.WELL));
        assertEquals(RoundResult.LOSS, Choice.WELL.playAgainst(Choice.PAPER));
    }
}