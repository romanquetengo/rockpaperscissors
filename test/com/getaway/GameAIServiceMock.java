package com.getaway;

import java.util.concurrent.atomic.AtomicInteger;

public class GameAIServiceMock implements GameAIService {
    private int mockMove;
    AtomicInteger invocations;

    public GameAIServiceMock(int mockMove) {
        this.invocations = new AtomicInteger(0);
        this.mockMove = mockMove;
    }

    public void setMove(int move) {
        this.mockMove = move;
    }

    @Override
    public int getNextMove(int possibleMoves) {
        invocations.getAndIncrement();
        return mockMove;
    }
}
