package com.getaway;

import junit.framework.TestCase;

public class MatchManagerTest extends TestCase {


    private GameAIServiceMock gameAIService;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        gameAIService = new GameAIServiceMock(0);
        MatchManager.SINGLETON.setGameAIService(gameAIService);
    }

    public void testNextChoice(){
        assertEquals(Choice.ROCK, MatchManager.SINGLETON.nextChoice());
        gameAIService.setMove(1);
        assertEquals(Choice.PAPER, MatchManager.SINGLETON.nextChoice());
        gameAIService.setMove(2);
        assertEquals(Choice.SCISSORS, MatchManager.SINGLETON.nextChoice());
        gameAIService.setMove(3);
        assertEquals(Choice.WELL, MatchManager.SINGLETON.nextChoice());
        gameAIService.setMove(4);
        try {
            MatchManager.SINGLETON.nextChoice();
            fail();
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    public void testPlayRound(){
        Round predefinedRound = new Round(1, Choice.PAPER, Choice.ROCK);
        MatchManager.SINGLETON.playRound(predefinedRound);
        assertTrue(predefinedRound.roundPlayed());
        assertEquals(RoundResult.WIN, predefinedRound.getResult());
        assertEquals(0, gameAIService.invocations.get());

        Round randomVersusFixedRound = new Round(1, null, Choice.ROCK);
        MatchManager.SINGLETON.playRound(randomVersusFixedRound);
        assertTrue(randomVersusFixedRound.roundPlayed());
        assertEquals(1, gameAIService.invocations.get());

        Round randomRound = new Round(1, null, null);
        MatchManager.SINGLETON.playRound(randomRound);
        assertTrue(randomRound.roundPlayed());
        assertEquals(3, gameAIService.invocations.get());
    }
}